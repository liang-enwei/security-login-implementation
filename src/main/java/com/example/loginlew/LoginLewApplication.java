package com.example.loginlew;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@SpringBootApplication
@MapperScan("com.example.loginlew.mapper")
public class LoginLewApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoginLewApplication.class, args);
    }

}
