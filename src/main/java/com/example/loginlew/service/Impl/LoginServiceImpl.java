package com.example.loginlew.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.loginlew.domain.RespResult;
import com.example.loginlew.domain.entity.LoginUser;
import com.example.loginlew.domain.entity.User;
import com.example.loginlew.domain.vo.UserLoginVO;
import com.example.loginlew.enums.AppHttpCodeEnum;
import com.example.loginlew.mapper.UserMapper;
import com.example.loginlew.service.LoginService;
import com.example.loginlew.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * description : 类描述
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
@Service
public class LoginServiceImpl implements LoginService  {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public RespResult login(User user) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());

        Authentication authenticate = authenticationManager.authenticate(authenticationToken);
        if (Objects.isNull(authenticate)){
            return RespResult.error(AppHttpCodeEnum.LOGIN_ERROR);
        }

        // 从这个里面把我们得用户和权限提出来
        LoginUser loginUser = (LoginUser) authenticate.getPrincipal();
        // 这个我们可以生成我们得token令牌，然后我们就可以做其他操作
        //System.out.println("loginUser----->" + loginUser);
        String userId = loginUser.getUser().getId().toString();

        String token = JwtUtil.createJWT(userId);
        //TODO 这里我们可以把我们得token作为key 用户信息作为value存放在redis

        //TODO 这里我们可以做用户信息剔除处理返回给前端

        return RespResult.success().putDataValue(new UserLoginVO(token, loginUser.getUser()));
    }

    @Override
    public RespResult regist(User user) {
        //TODO 这里可以对每个属性进行非空判断
        String password = passwordEncoder.encode(user.getPassword());
        user.setPassword(password);
        int insert = userMapper.insert(user);
        return RespResult.success().putDataValue(user);
    }
}
