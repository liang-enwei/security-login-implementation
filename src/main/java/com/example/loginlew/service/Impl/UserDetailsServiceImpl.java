package com.example.loginlew.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.loginlew.domain.entity.LoginUser;
import com.example.loginlew.domain.entity.User;
import com.example.loginlew.enums.AppHttpCodeEnum;
import com.example.loginlew.mapper.UserMapper;
import com.example.loginlew.util.SystemConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * description : 类描述
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getUsername, username);
        User user = userMapper.selectOne(wrapper);

        if (Objects.isNull(user)){
            throw new RuntimeException(AppHttpCodeEnum.LOGIN_ERROR.getMsg());
        }

        if (user.getUserType().equals(SystemConstants.ADMIN)){
            //TODO 这里可以采用数据库封装权限列表
            return new LoginUser(user, "admin");
        }
        return new LoginUser(user, null);
    }
}
