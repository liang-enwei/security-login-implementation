package com.example.loginlew.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.loginlew.domain.RespResult;
import com.example.loginlew.domain.entity.User;
import org.springframework.stereotype.Service;

/**
 * description : 类描述
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
public interface LoginService {
    RespResult login(User user);

    RespResult regist(User user);
}
