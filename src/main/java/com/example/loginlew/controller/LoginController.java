package com.example.loginlew.controller;

import com.example.loginlew.domain.RespResult;
import com.example.loginlew.domain.entity.User;
import com.example.loginlew.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * description : 登录接口
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @RequestMapping("/login")
    public RespResult login(@RequestBody User user){
        if (!StringUtils.hasText(user.getUsername())){
            return RespResult.error("请输入用户名");
        }
        System.err.println(user.toString());
        return loginService.login(user);
    }

    @RequestMapping("/regist")
    public RespResult regist(@RequestBody User user){
        if (!StringUtils.hasText(user.getUsername())){
            return RespResult.error("请输入用户名");
        }
        return loginService.regist(user);
    }

    public RespResult logout(){
        //TODO 我们需要做的就是把我们存放在redis中得登录转台删除掉
        return RespResult.success();
    }
}
