package com.example.loginlew.controller;

import com.example.loginlew.domain.RespResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;

/**
 * description : 测试类
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
@Controller
public class Hello {

    @GetMapping("/hello")
    @ResponseBody
    public RespResult test(){
        ArrayList<String> list = new ArrayList<>();
        list.add("111");
        list.add("222");
        list.add("333");

        return RespResult.success().putDataValue(list);
    }
}
