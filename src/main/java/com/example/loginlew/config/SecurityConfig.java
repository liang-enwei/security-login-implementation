package com.example.loginlew.config;

import com.example.loginlew.filter.JwtAuthenticationTokenFilter;
import com.example.loginlew.handler.security.AccessDeniedHandlerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * description : 类描述
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AccessDeniedHandler accessDeniedHandler;

    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;

    @Resource
    private DataSource dataSource;

    @Override
    @Bean
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    // 记住登录配置
    //@Bean
    //public PersistentTokenRepository persistentTokenRepository() {
    //    JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
    //    jdbcTokenRepository.setDataSource(dataSource);
    //    return jdbcTokenRepository;
    //}

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                    .disable()
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // 允许跨域
                .cors()
                .and()
                // 权限放行
                .authorizeRequests()
                    // 登录接口允许匿名访问
                    .antMatchers("/login").anonymous()
                    // 注销接口需要认证才能访问
                    .antMatchers("/logout").authenticated()
                    // 需要管理员权限才能访问
                    .antMatchers("/admin/**").hasAnyAuthority("admin")
                    .antMatchers("/user/**").authenticated()
                    // 除上面外的所有请求全部不需要认证即可访问
                    .anyRequest().permitAll()
                .and()
                // 配置处理器
                .exceptionHandling()
                    // 认证失败处理器
                    .authenticationEntryPoint(authenticationEntryPoint)
                    // 授权失败拒绝访问处理器
                    .accessDeniedHandler(accessDeniedHandler)
                .and()
                // 配置登录等率过滤器
                .addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);

        // 关闭默认的注销功能
        http.logout().disable();
        // 记住登录配置 要陪有相关得数据库官方获取
        //http
        //        .rememberMe()
        //        .tokenRepository(persistentTokenRepository())
        //        .tokenValiditySeconds(60*60)
        //        .userDetailsService(userDetailsService());

    }
}
