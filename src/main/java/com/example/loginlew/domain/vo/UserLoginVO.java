package com.example.loginlew.domain.vo;

import com.example.loginlew.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * description : 用户前端传输类
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginVO {
    private String token;
    private User user;
}
