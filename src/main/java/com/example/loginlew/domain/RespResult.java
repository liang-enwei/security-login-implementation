package com.example.loginlew.domain;

import com.example.loginlew.enums.AppHttpCodeEnum;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * description : 所有返回结果统一封装返回
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
public class RespResult<T> implements Serializable {

    private static final long serialVersionUID = 1L;
    private final Map<String, Object> mapData = new HashMap<>();

    private int code;
    private String msg;
    private T data;
    

    /**
     * description 使用枚举得状态码构造方法
     * @author 沐光
     */
    public RespResult() {
        this.code = AppHttpCodeEnum.SUCCESS.getCode();
        this.msg = AppHttpCodeEnum.SUCCESS.getMsg();
    }

    /**
     * description 没有数据返回得封装
     * @author 沐光
     */
    public RespResult(int code, String message){
        this.code = code;
        this.msg = message;
    }

    /**
     * description 添加数据自定义key
     * @author 沐光
     */
    public RespResult<T> putDataValue(String key, T data){
        mapData.put(key, data);
        this.data = (T) mapData;
        return this;
    }
    /**
     * description 添加数据默认key data
     * @author 沐光
     */
    public RespResult<T> putDataValue(T data){
        return this.putDataValue("data",data);
    }

    public static <T> RespResult<T> success(int code, String msg){
        return new RespResult<T>(code, msg);
    }

    public static <T> RespResult<T> success(){
        return new RespResult<>();
    }

    public static <T> RespResult<T> error(int code, String msg){
        return new RespResult<T>(code, msg);
    }

    public static <T> RespResult<T> error(String msg){
        return error(AppHttpCodeEnum.SYSTEM_ERROR.getCode(), msg);
    }

    public static <T> RespResult<T> error(AppHttpCodeEnum codeEnum){
        return new RespResult<>(codeEnum.getCode(), codeEnum.getMsg());
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
