package com.example.loginlew.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * description : 类描述
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
@Data
@NoArgsConstructor
public class LoginUser implements UserDetails {

    private User user;
    private List<String> permissions;

    /**
     * description 这个是我们直接赋予权限字段标志
     * @author 沐光
     * @date 2024/3/1
     */
    public LoginUser(User user, String permissions) {
        this.user = user;
        this.permissions = Arrays.asList(new String[]{permissions});
    }

    /**
     * description 这个权限需要我们从数据库中查询出来
     * @author 沐光
     * @date 2024/3/1
     */
    //public LoginUser(User user, List<String> permissions) {
    //    this.user = user;
    //    this.permissions = permissions;
    //}

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return permissions.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getPassword();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
