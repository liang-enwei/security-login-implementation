package com.example.loginlew.util;

import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * description : 类描述
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
public class BeanCopyUtils {

    public static <T> T copyBean(Object source, Class<T> clazz) {
        T result = null;
        try {
            result = clazz.newInstance();
            BeanUtils.copyProperties(source, result);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static <K, V> List<V> copyBeanList(List<K> list, Class<V> clazz) {
        return list.stream()
                .map(element -> copyBean(element, clazz))
                .collect(Collectors.toList());
    }
}
