package com.example.loginlew.util;

import com.example.loginlew.domain.entity.LoginUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * description : security工具类
 *
 * @author : 沐光
 * @date : 2024-03-02
 */
public class SecurityUtils {

    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public static LoginUser getLoginUser(){
        return (LoginUser) getAuthentication().getPrincipal();
    }

    public static Long getUserId() {
        return getLoginUser().getUser().getId();
    }
}
