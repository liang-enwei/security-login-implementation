package com.example.loginlew.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.loginlew.domain.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * description: 类描述
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
