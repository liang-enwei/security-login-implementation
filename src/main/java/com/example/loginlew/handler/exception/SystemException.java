package com.example.loginlew.handler.exception;


import com.example.loginlew.enums.AppHttpCodeEnum;

/**
 * @Description: 自定义异常信息封装类
 * @Author: 沐光
 */
public class SystemException extends RuntimeException{

    private int code;

    private String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public SystemException(AppHttpCodeEnum httpCodeEnum) {
        super(httpCodeEnum.getMsg());
        this.code = httpCodeEnum.getCode();
        this.msg = httpCodeEnum.getMsg();
    }
}
