package com.example.loginlew.handler.exception;

import com.example.loginlew.domain.RespResult;
import com.example.loginlew.enums.AppHttpCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * description : 全局异常处理统一返回
 *
 * @author : 沐光
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = SystemException.class)
    public <T> RespResult<T> systemExceptionHandler(SystemException e){
        log.error("出现了异常 SystemException ", e);
        return RespResult.error(e.getCode(), e.getMsg());
    }

    @ExceptionHandler(value = Exception.class)
    public <T> RespResult<T> exceptionHandler(Exception e){
        log.error("出现了异常 Exception ", e);
        return RespResult.error(AppHttpCodeEnum.SYSTEM_ERROR.getCode(), e.getMessage());
    }
}
