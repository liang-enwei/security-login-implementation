package com.example.loginlew.handler.security;

import com.alibaba.fastjson2.JSON;
import com.example.loginlew.domain.RespResult;
import com.example.loginlew.enums.AppHttpCodeEnum;
import com.example.loginlew.util.WebUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * description : 授权失败处理器
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
@Component
public class AccessDeniedHandlerImpl implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {

        accessDeniedException.printStackTrace();
        RespResult<Object> result = RespResult.error(AppHttpCodeEnum.NO_OPERATOR_AUTH);
        //response.setStatus(HttpServletResponse.SC_OK);
        //response.setContentType("application/json");
        //response.setCharacterEncoding("UTF-8");
        //PrintWriter writer = response.getWriter();
        //writer.write(JSON.toJSONString(result));
        //writer.flush();
        //writer.close();

        WebUtils.renderString(response, JSON.toJSONString(result));

    }
}
