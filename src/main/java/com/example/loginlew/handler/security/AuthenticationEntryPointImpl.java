package com.example.loginlew.handler.security;

import com.alibaba.fastjson2.JSON;
import com.example.loginlew.domain.RespResult;
import com.example.loginlew.enums.AppHttpCodeEnum;
import com.example.loginlew.util.WebUtils;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * description : 认证失败处理器
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {

        authException.printStackTrace();

        RespResult result = null;
        // 在这里判断异常信息是哪一种类型然后做出处理
        if (authException instanceof BadCredentialsException) {
            // 封装错误格式
            result = RespResult.error(AppHttpCodeEnum.LOGIN_ERROR.getCode(), authException.getMessage());
        } else if (authException instanceof InsufficientAuthenticationException) {
            result = RespResult.error(AppHttpCodeEnum.NEED_LOGIN);
        } else {
            result = RespResult.error(AppHttpCodeEnum.SYSTEM_ERROR.getCode(), "认证或授权出现问题");
        }

        //response.setStatus(HttpServletResponse.SC_OK);
        //response.setContentType("application/json");
        //response.setCharacterEncoding("UTF-8");
        //PrintWriter writer = response.getWriter();
        //writer.write(JSON.toJSONString(result));
        //writer.flush();
        //writer.close();

        WebUtils.renderString(response, JSON.toJSONString(result));


    }
}
