package com.example.loginlew.filter;

import com.alibaba.fastjson2.JSON;
import com.example.loginlew.domain.RespResult;
import com.example.loginlew.enums.AppHttpCodeEnum;
import com.example.loginlew.util.JwtUtil;
import com.example.loginlew.util.WebUtils;
import io.jsonwebtoken.Claims;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * description : token过滤器
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = request.getParameter("token");
        if (!StringUtils.hasText(token)){
            filterChain.doFilter(request,response);
            return;
        }

        Claims claims = null;
        try {
            claims = JwtUtil.parseJWT(token);
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
            // token超时 token非法
            // 响应告诉前端需要重新登录
            RespResult result = RespResult.error(AppHttpCodeEnum.NEED_LOGIN);
            // 使用工具类来封装返回格式
            WebUtils.renderString(response, JSON.toJSONString(result));
            return;
        }

        String userId = claims.getSubject();
        // 从redis中获取用户信息
        //LoginUser loginUser = redisCache.getCacheObject("login:" + userId);
        // 如果获取不到数据
        //if (Objects.isNull(loginUser)) {
        //    // 说明登录过期token失效 提示重新登录
        //    ResponseResult result = ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        //    WebUtils.renderString(response, JSON.toJSONString(result));
        //    return;
        //}
        // 用户信息存入SecurityContextHolder
        //UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser, null, null);
        //SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        //System.err.println("blog:SecurityContextHolder--->" + SecurityContextHolder.getContext().getAuthentication().getPrincipal());

        // 放行
        filterChain.doFilter(request, response);
    }
}
