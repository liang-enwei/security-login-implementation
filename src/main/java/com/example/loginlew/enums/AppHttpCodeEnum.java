package com.example.loginlew.enums;

/**
 * description : 统一返回状态码
 *
 * @author : 沐光
 * @date : 2024-03-01
 */
public enum AppHttpCodeEnum {

    SUCCESS(200,"操作成功"),
    NEED_LOGIN(401,"需要登录后操作"),
    NO_OPERATOR_AUTH(403,"无权限操作"),
    SYSTEM_ERROR(500,"出现错误"),
    LOGIN_ERROR(505,"用户名或密码错误"),
    USERNAME_EXIST(501,"用户名已存在");

    int code;
    String msg;

    AppHttpCodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }


    public String getMsg() {
        return msg;
    }

}
